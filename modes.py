import numpy as np
from mathutils import cart2pol, pol2cart

from numba import jit
import math as pmath
import cupy as cp
import numexpr as ne

import matplotlib.pyplot as plt
from plots import (start_qt_app, QtSingleFieldStack, QtMPLCFieldStack, ComplexArrayToRgb)

from numpy.fft import fftshift, ifftshift, fft2, ifft2

import time


class MODES():
    def __init__(self, maxMG, X, Y, MFD, basis = 'HG', tilted = False, theta = 0, tilt_axis = 'x', xc = 0, yc = 0, z_offset = 0, wavelength = 1565e-9, 
                meshgrid_rotation_angle = 0, use_gpu = False):
                
        self.maxMG = maxMG
        self.MFD = MFD
        self.W = self.MFD/2
        self.w_scaled = self.W/2**.5
        self.wavelength = wavelength # Needed for tilted plane modes as these are calculated with the z_offset and z-propagation depends on wavelength
        self.meshgrid_rotation_angle = meshgrid_rotation_angle

        # I need to remember the orginal coordinates in order to subtract the tilt phase later on
        self.Xo = X
        self.Yo = Y
        
        
        if tilted == False: # A simple coordinate system is used for modes that are calced on a plane with (z=0, perpendicular to the mode propagation)

            # Rotating modes
            self.R, self.PHI = cart2pol(X - xc, Y - yc)
            self.X_rot, self.Y_rot = pol2cart(self.R, self.PHI - self.meshgrid_rotation_angle)
            
            self.X = self.X_rot
            self.Y = self.Y_rot
            
            self.Nx = self.X.shape[0] 
            self.Ny = self.X.shape[1] 
            self.R, self.PHI = cart2pol(self.X, self.Y)
            
            #! GPU calc parameters (so far only for HG modes)
            if use_gpu:
                self.X_gpu = cp.asarray(self.X.astype(np.float32)/self.w_scaled)
                self.Y_gpu = cp.asarray(self.Y.astype(np.float32)/self.w_scaled)

                self.out = cp.empty((self.Nx,self.Ny), dtype=cp.complex64)
                
                dx = (self.Yo[1] - self.Yo[0])[0]
                self.HG_sf = dx/self.w_scaled
                

        elif tilted == True: # A coordinate transform is needed to calculate the mode profile on a tilted plane (angle ... theta, ... distance z_offset, orientation ... tilt_axis)
            if tilt_axis == 'x':
                self.X = np.cos(theta)*(X - xc)
                self.Y = Y - yc
                self.Z = np.sin(theta)*X + z_offset

                self.Nx = self.X.shape[0] 
                self.Ny = self.X.shape[1] 
                self.R, self.PHI = cart2pol(self.X, self.Y)
            elif tilt_axis == 'y':
                self.X = X - xc
                self.Y = np.cos(theta)*(Y - yc)
                self.Z = np.sin(theta)*Y + z_offset

                self.Nx = self.X.shape[0] 
                self.Ny = self.X.shape[1] 
                self.R, self.PHI = cart2pol(self.X, self.Y)  
        
        # The code below is not affected by the tilt change
        self.mode_count = np.sum(range(self.maxMG + 1)) 
        self.MODES = np.zeros((self.mode_count, self.Nx, self.Ny), dtype = np.complex64)
        self.M = np.zeros(self.mode_count)
        self.N = np.zeros(self.mode_count)
        self.TOTAL_MODES = np.zeros((self.Nx, self.Ny), dtype = np.complex64)

    
        if basis == 'HG':
            idx = 0
            for mgIdx in range(maxMG):
                for modeIdx in range(mgIdx + 1):
                    m = mgIdx - modeIdx
                    n = mgIdx - m

                    if tilted == False:
                        if use_gpu:
                            self.MODES[idx,:,:] = self.HG_mode_gpu(m, n)
                        else:    
                            self.MODES[idx,:,:] = self.HG_mode(m, n)
                    elif tilted == True:
                        self.MODES[idx,:,:] = self.HG_mode_tilted(m, n, theta, tilt_axis, z_offset, xc, yc)

                    self.M[idx] = m
                    self.N[idx] = n

                    self.TOTAL_MODES += (np.abs(self.MODES[idx,:,:]))**2
                    idx += 1
        else:   
            L = np.arange(0, maxMG + 1, 1)
            MM = np.arange(1, maxMG + 1, 1)
            L, MM = np.meshgrid(L, MM,sparse = False, indexing = 'xy') # This spans the whole mode group parametric space

            mode_group = np.transpose(2*MM + L - 1) # individual mode groups are in rows
            mode_group = mode_group.flatten() # making one long vector 

            mg = np.sort(mode_group)
            mgIdx = np.argsort(mode_group, kind='mergesort')
            idxes = np.where(mg <= maxMG) # finds indices L, M for modes in the mode group

            L = np.transpose(L)
            MM = np.transpose(MM)
            L = L.flatten()
            MM = MM.flatten()
            L = L[mgIdx[idxes]]
            MM = MM[mgIdx[idxes]]
            
            idx = 0

            for lIdx in range(len(L)):
                l = L[lIdx]
                m = MM[lIdx]
                if basis == 'LG':
                    Ex, Ey = self.LG_mode(l, m)
                elif basis == 'LP':
                    Ex, Ey = self.LP_mode(l, m)
                    
                self.MODES[idx] = Ex
                self.M[idx] = l
                self.N[idx] = m
                self.TOTAL_MODES += (np.abs(Ex))**2
                idx += 1
                if l>0:  
                    self.MODES[idx] = Ey
                    self.M[idx] = l
                    self.N[idx] = m
                    idx += 1
                    self.TOTAL_MODES += (np.abs(Ey))**2              




    ''' ================================== (START) HG mode calc related =========================================================='''
    
    #! (START) HG mode CPU implementation
    #! =========================================================
    def HermitePoly(self, n, x):
            p = []
            p.append([1])

            if n>0:
                p.append([2,0])
            if n>1:     
                [p.append([2*(a-(k-1)*b) for a,b in zip(p[k-1]+[0], 2*[0] + p[k-2])]) for k in range(2,n+1)]

            p2 = p[-1]
            y = np.polyval(p2,x)

            return y

    def HG_mode(self, l, m):

        u = np.sqrt(2)*self.X/self.W
        v = np.sqrt(2)*self.Y/self.W
        
        G_l = self.HermitePoly(l,u)*np.exp(-u**2/2)
        G_m = self.HermitePoly(m,v)*np.exp(-v**2/2)
        Ex = G_l*G_m
        Ex = Ex/(np.sqrt(np.sum(np.absolute(Ex)**2)))

        return Ex
    

    def HG_mode_tilted(self, l, m, theta, tilt_axis, z_offset, xc, yc):

        k = 2*np.pi/self.wavelength

        # Rotate the already tilted coordinates so that we get the mode stretched along the proper 'tilt_axis'
        R, PHI = cart2pol(self.X, self.Y)
        self.X_rot, self.Y_rot = pol2cart(R, PHI - self.meshgrid_rotation_angle)
        r2 = self.X_rot**2 + self.Y_rot**2

        zr = np.pi*self.W**2/self.wavelength
        Wz = (self.W*np.sqrt(1 + (self.Z/zr)**2)).astype(np.float64) 
        guoy_phase = np.arctan(self.Z/zr).astype(np.float64)
        zr_div_Z = np.divide(zr, self.Z, out=np.zeros(self.Z.shape, dtype=np.float64), where = self.Z!=0) # gives 0 where z = 0, otherwise zr/z
        Rz = self.Z*(1 + (zr_div_Z)**2)
        Rzi = np.divide(1, Rz, out=np.zeros(Rz.shape, dtype=np.float64), where = Rz!=0) # gives 0 where Rz = 0, otherwise 1/Rz  
        
        # Using the tilted & and now also rotated coordinates
        u = np.sqrt(2)*self.X_rot/Wz
        v = np.sqrt(2)*self.Y_rot/Wz

        G_l = self.HermitePoly(l,u)*np.exp(-u**2/2)
        G_m = self.HermitePoly(m,v)*np.exp(-v**2/2)

        field = (self.W/Wz)*G_l*G_m*np.exp(-1j*k*self.Z - 1j*k*r2*Rzi/2 + 1j*(l + m + 1)*guoy_phase)

        # Subtract the tilt from the field. NOTE: using the original untilted coordinates
        if tilt_axis == 'x':
            field *= np.exp(+1j*k*np.sin(theta)*self.Xo)                    
        elif tilt_axis == 'y':
            field *= np.exp(+1j*k*np.sin(theta)*self.Yo)

        field /= np.linalg.norm(field)  

        return field
    
    #! (STOP) HG mode CPU implementation
    #! =========================================================
    
    
    #! (START) HG mode GPU implementation
    #! =========================================================
    def HG_mode_gpu(self, m, n):
        
        HG_function = """
        inline __device__ float HG_function(float x, int n)
        {
            float h2 = 0.7511255444649425f;
            float h1 = x*1.062251932027197f;
            float h;
            float scale;
            float log_scale;
            float sum_log_scale = 0.f;
            
            if (n==0)
            {
                return 0.7511255444649425f*expf(-x*x/2);
            }
            if (n==1)
            {
                return 1.062251932027197f*x*expf(-x*x/2);
            }
            
            if (x==0.f)
            {
            x=0.00000001f;
            }
            
            for (int i=2;i<n+1;i++)
            {
                h=sqrtf(2.0f/i)*x*h1-sqrtf((i-1.)/i)*h2;
                h2=h1;
                h1=h;
                
                log_scale=round(logf(abs(h)));
                scale=expf(-log_scale);
                h  *= scale;
                h1 *= scale;
                h2 *= scale;
                sum_log_scale+=log_scale;
            }
            return h*expf(-x*x/2+sum_log_scale);
        }
        """    
        
        HG_mode_2D = cp.ElementwiseKernel(  
                    "float32 x_rotated, float32 y_rotated, float32 HG_sf, int32 m, int32 n", 
                    "complex64 HG_mode_out",
                    """
                    HG_mode_out = HG_sf*HG_function(x_rotated, n)*HG_function(y_rotated, m);
                    """,
                    'HG_mode_2D',preamble=HG_function)
        
        #* Call the GPU kernel
        HG_mode_2D(self.X_gpu, self.Y_gpu, self.HG_sf, m, n, self.out[:self.Nx,:self.Ny])
        
        return cp.asnumpy(self.out)
    
    #! (STOP) HG mode GPU implementation
    #! =========================================================

    ''' ================================== (STOP) HG mode calc related =========================================================='''




    def LaguerrePoly(self, n, alpha):
        coeff = []
        factorial_1 = np.math.factorial(n + alpha)
        bino = []
        [bino.append(factorial_1/(np.math.factorial(n-m)*np.math.factorial((n+alpha)-(n-m)))) for m in range(n+1)]
        [coeff.append(bino[m] * (-1)**(m)/(np.math.factorial(m))) for m in range(n,-1,-1)] 

        return coeff

    def LG_mode(self, l, m):
        
        W = self.MFD/(2*np.sqrt(2))
        V = 1/(W**2)
        LG = self.LaguerrePoly(m-1, l)
        y = np.polyval(LG, V*(self.R**2))
        F_lm = (self.R**l) * y * np.exp(-0.5*V*(self.R**2))
        l_PHI = l*self.PHI

        Ex = F_lm * np.cos(l_PHI)
        Ey = F_lm * np.sin(l_PHI)

        ex = Ex + 1j*Ey
        ey = Ex - 1j*Ey

        norm_ex = np.sqrt(np.sum(np.absolute(ex)**2))
        norm_ey = np.sqrt(np.sum(np.absolute(ey)**2))

        if norm_ex != 0:
            ex = ex/norm_ex
        if norm_ey!= 0:    
            ey = ey/norm_ey

        return (ex, ey)

    def LP_mode(self, l, m):
        
        W = self.MFD/(2*np.sqrt(2))
        V = 1/(W**2)
        LG = self.LaguerrePoly(m-1, l)
        y = np.polyval(LG, V*(self.R**2))
        F_lm = (self.R**l) * y * np.exp(-0.5*V*(self.R**2))
        l_PHI = l*self.PHI   

        ex = F_lm*np.cos(l_PHI)
        ey = F_lm*np.sin(l_PHI)
        
        norm_ex = np.linalg.norm(ex)
        norm_ey = np.linalg.norm(ey)

        if norm_ex != 0:
            ex /= norm_ex
        if norm_ey!= 0:    
            ey /= norm_ey

        return (ex, ey)
    
    

# =============================== SLM based mode filter ==========================================================================
class mode_SLM_filter():
    def __init__(self, X, Y, MFD, l = 0, m = 1, rotation_angle = 0, xc = 0, yc = 0, basis = 'LG', stretch_X = 1, stretch_Y = 1):

        self.MFD = MFD
        self.W = self.MFD/2
        self.rotation_angle = rotation_angle

        self.R, self.PHI = cart2pol(X - xc, Y - yc) # Mode offset from center
        self.X_rot, self.Y_rot = pol2cart(self.R, self.PHI - self.rotation_angle) # Rotate modes

        self.X = self.X_rot
        self.Y = self.Y_rot

        self.Nx = self.X.shape[0] 
        self.Ny = self.X.shape[1] 
        self.R, self.PHI = cart2pol(stretch_Y*self.X, stretch_X*self.Y) # NOTE: I flip the stretch coordinates to correspond to plotting (for convenience)


        if basis == 'LG':
            Ex, Ey = self.LG_mode(np.abs(l),m)
        elif basis == 'LP':
            Ex, Ey = self.LP_mode(np.abs(l),m) 

        if np.sign(l) == 1:
            self.E = Ex
        elif np.sign(l) == -1:
            self.E = Ey
        else:
            self.E = Ex # in cases when l = 0 


    def LaguerrePoly(self, n, alpha):
        coeff = []
        factorial_1 = np.math.factorial(n + alpha)
        bino = []
        [bino.append(factorial_1/(np.math.factorial(n-m)*np.math.factorial((n+alpha)-(n-m)))) for m in range(n+1)]
        [coeff.append(bino[m] * (-1)**(m)/(np.math.factorial(m))) for m in range(n,-1,-1)] 

        return coeff

    def LG_mode(self, l, m):
        
        W = self.MFD/(2*np.sqrt(2))
        V = 1/(W**2)
        LG = self.LaguerrePoly(m-1, l)
        y = np.polyval(LG, V*(self.R**2))
        F_lm = (self.R**l) * y * np.exp(-0.5*V*(self.R**2))
        l_PHI = l*self.PHI

        Ex = F_lm * np.cos(l_PHI)
        Ey = F_lm * np.sin(l_PHI)

        ex = Ex + 1j*Ey
        ey = Ex - 1j*Ey

        norm_ex = np.sqrt(np.sum(np.absolute(ex)**2))
        norm_ey = np.sqrt(np.sum(np.absolute(ey)**2))

        if norm_ex != 0:
            ex = ex/norm_ex
        if norm_ey!= 0:    
            ey = ey/norm_ey

        return (ex, ey)        

    def LP_mode(self, l, m):
        
        W = self.MFD/(2*np.sqrt(2))
        V = 1/(W**2)
        LG = self.LaguerrePoly(m-1, l)
        y = np.polyval(LG, V*(self.R**2))
        F_lm = (self.R**l) * y * np.exp(-0.5*V*(self.R**2))
        l_PHI = l*self.PHI   

        ex = F_lm*np.cos(l_PHI)
        ey = F_lm*np.sin(l_PHI)
        
        norm_ex = np.linalg.norm(ex)
        norm_ey = np.linalg.norm(ey)

        if norm_ex != 0:
            ex /= norm_ex
        if norm_ey!= 0:    
            ey /= norm_ey    

        return (ex, ey)            
# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


def kspaceKernel(k0, theta, KX, KY, kref = None):
    '''Make a kspace kernel with k0,theta,KX,KY
    when the planes are tilted, we propagate around theta.
    tilt in the xdirection. (last axis)
    '''

    if kref is None:
        kref = k0

    kxshift = np.sin(        theta) * k0  #how shifted we are in kspace for tilted planes

    kz = ((k0**2 - (KX + kxshift)**2 - KY**2)**.5  #main part of kernel
                + np.tan(theta) * (KX + kxshift) #shifted from the phase
                -kref) #no constant phase of kernel.
    kz[np.isnan(kz)] = 0.

    return kz#np.fft.fftshift(kz)
    
    



if __name__ == "__main__":
    print('Module runs fine...')

    # # ======================  Making the filter mode ================================================================================
    # MFD_factor = 0.5 # For 1 the mode field diameter will cover the whole generated MASK

    # SLM_pixel_size = 9.2e-6
    # mask_size_x = 1024
    # mask_size_y = 1024
    # downsample = 1

    # MFD = mask_size_x*SLM_pixel_size*MFD_factor

    # pixel_size = downsample*SLM_pixel_size
    # Nx = 2*int(np.floor(mask_size_x/(2*downsample)))
    # Ny = 2*int(np.floor(mask_size_y/(2*downsample)))

    # sampling_X, step_X = np.linspace((pixel_size)*(-Nx/2 + 0.5), (pixel_size)*(Nx/2 - 0.5), Nx, dtype = np.float32, retstep = True) 
    # sampling_Y, step_Y = np.linspace((pixel_size)*(-Ny/2 + 0.5), (pixel_size)*(Ny/2 - 0.5), Ny, dtype = np.float32, retstep = True)


    # X, Y = np.meshgrid(sampling_X, sampling_Y)

    # l = 3
    # m = 1
    

    # mode = mode_SLM_filter(X, Y, MFD, l = l, m = m, rotation_angle = 0, xc = 0, yc = 0, basis = 'LP', stretch_X = 1.5, stretch_Y = 0.7) 
    # fig, ax = plt.subplots(nrows = 1, ncols = 1)
    # fig.set_size_inches(5, 4)

    # # print(mode.phase_filter.min())

    # ax.imshow(ComplexArrayToRgb(mode.E))
    # plt.show()